import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookAFlightComponent } from './component/book-aflight/book-aflight.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageService } from './share/page.service';

@NgModule({
  declarations: [
    AppComponent,
    BookAFlightComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
  ],
  providers: [
    PageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
