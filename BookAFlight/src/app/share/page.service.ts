import { Injectable } from '@angular/core';
import { Flight } from '../component/flight';
import { Mockdata } from './mockdata';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights: Flight[] = [];

  constructor() {
    this.flights = Mockdata.mflight;
  }

  getFlight(): Flight[] {
    return this.flights;
  }

  addFlight(f: Flight): void{
    this.flights.push(f)
  }
}
