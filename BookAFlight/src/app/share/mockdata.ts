import { Flight } from "../component/flight";

export class Mockdata {
  public static mflight: Flight[] =[
    {
       fullName: "Ironman",
       from: "Bangkok",
       to: "Tokyo",
       type: "One-way",
       adults: 1,
       departure: new Date('2022, 01, 01'),
       children: 2,
       infants: 3,
       arrival: new Date('2022, 01, 15'),
    },
    {
       fullName: "Spider",
       from: "New York",
       to: "London",
       type: "Return",
       adults: 3,
       departure: new Date('2022, 02, 14'),
       children: 2,
       infants: 1,
       arrival: new Date('2022, 02, 17'),
    }
  ];
}
