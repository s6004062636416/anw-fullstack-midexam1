import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Flight } from '../flight';
import {
  NgbDateStruct,
  NgbCalendar,
  NgbDatepickerI18n,
  NgbCalendarBuddhist,
  NgbDate,
  NgbDateParserFormatter
} from '@ng-bootstrap/ng-bootstrap';
import localeThai from '@angular/common/locales/th';
import { getLocaleDayNames, FormStyle, TranslationWidth, getLocaleMonthNames, formatDate, registerLocaleData } from '@angular/common';
import { PageService } from 'src/app/share/page.service';

@Injectable()
export class NgbDatepickerI18nBuddhist extends NgbDatepickerI18n {

  private _locale = 'th';
  private _weekdaysShort: readonly string[];
  private _monthsShort: readonly string[];
  private _monthsFull: readonly string[];

  constructor() {
    super();

    registerLocaleData(localeThai);

    const weekdaysStartingOnSunday = getLocaleDayNames(this._locale, FormStyle.Standalone, TranslationWidth.Short);
    this._weekdaysShort = weekdaysStartingOnSunday.map((day, index) => weekdaysStartingOnSunday[(index + 1) % 7]);

    this._monthsShort = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Abbreviated);
    this._monthsFull = getLocaleMonthNames(this._locale, FormStyle.Standalone, TranslationWidth.Wide);
  }

  getMonthShortName(month: number): string { return this._monthsShort[month - 1] || ''; }

  getMonthFullName(month: number): string { return this._monthsFull[month - 1] || ''; }

  getWeekdayLabel(weekday: number) {
    return this._weekdaysShort[weekday - 1] || '';
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    const jsDate = new Date(date.year, date.month - 1, date.day);
    return formatDate(jsDate, 'fullDate', this._locale);
  }

  override getYearNumerals(year: number): string { return String(year); }
}

@Component({
  selector: 'app-book-aflight',
  templateUrl: './book-aflight.component.html',
  styleUrls: ['./book-aflight.component.css'],
  providers: [
    { provide: NgbCalendar, useClass: NgbCalendarBuddhist },
    { provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBuddhist },
  ],
})
export class BookAFlightComponent implements OnInit {

  flightData !: Flight;
  flightList !: Flight[];
  form !: FormGroup;

  today: Date = new Date();

  hoveredDate: NgbDate | null = null;

  fromDate: NgbDate | null;
  toDate: NgbDate | null;

  constructor(private pageService: PageService,private fb: FormBuilder, private calendar: NgbCalendar, public formatter: NgbDateParserFormatter) {

    this.fromDate = calendar.getToday();
    this.toDate = calendar.getToday();

    this.flightList = this.pageService.getFlight();

    this.flightData = new Flight("", "", "", "", 0, this.today, 0, 0, this.today);
  }


  ngOnInit(): void {

    this.form = this.fb.group({
      fullName: ["", [Validators.required]],
      from: ["", [Validators.required]],
      to: ["", [Validators.required]],
      type: ["", [Validators.required]],
      adults: ["", [Validators.required]],
      children: ["", [Validators.required]],
      infants: ["", [Validators.required]],
    })

  }


  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;

      this.flightData.departure = new Date(date.year, date.month, date.day);

    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;

      this.flightData.arrival = new Date(date.year, date.month, date.day);

    } else {
      this.toDate = null;
      this.fromDate = date;

      this.flightData.arrival = new Date(date.year, date.month, date.day);
      this.flightData.departure = new Date(date.year, date.month, date.day);
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) &&
        date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) { return this.toDate && date.after(this.fromDate) && date.before(this.toDate); }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) ||
        this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  onSubmit(f:FormGroup): void{

    if(this.form.value.from == this.form.value.to){
      alert("You can't departure same place with arrival!");
      return;
    }else if(this.flightData.departure == this.flightData.arrival){
      alert("You can't departure same time with arrival!");
      return;
    }else{
      if(this.form.value.adults > 0 || this.form.value.children > 0 || this.form.value.adults > 0){
        alert("Book a Flight Success!");
        let form_record = new Flight(
          this.form.value.fullName,
          this.form.value.from,
          this.form.value.to,
          this.form.value.type,
          this.form.value.adults,
          this.flightData.departure,
          this.form.value.children,
          this.form.value.infants,
          this.flightData.arrival);

        this.pageService.addFlight(form_record);
      }else{
        alert("Need passenger at less 1 person");
        return;
      }
    }
  }
}
